const express = require('express');
const router = express.Router();

const ordersController = require('../controllers/orderController');

router.get('/', ordersController.order_get);

module.exports = router;
