const express = require('express');
const router = express.Router();

const productCategoryController = require('../controllers/productCategoryController');

router.get('/', productCategoryController.product_category_get);

module.exports = router;