const express = require('express');
const router = express.Router();

const productOptionController = require('../controllers/productOptionController');

router.get('/', productOptionController.product_option_get);

module.exports = router;