const express = require('express');
const router = express.Router();

const orderDetailController = require('../controllers/orderDetailController');

router.get('/', orderDetailController.order_detail_get);

module.exports = router;