const express = require('express');
const router = express.Router();

const optionController = require('../controllers/optionController');

router.get('/', optionController.option_get);

module.exports = router;