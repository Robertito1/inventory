const express = require('express');
const router = express.Router();

const customersController = require('../controllers/customerController');

router.get('/', customersController.customer_get);

module.exports = router;
