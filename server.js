
const createError = require('http-errors');


const express = require('express');


const path = require('path'); 


const indexRouter = require('./routes/index');
const productRouter = require('./routes/products');
const customerRouter = require('./routes/customers');
const orderRouter = require('./routes/orders');
const optionRouter = require('./routes/options');
const categoryRouter = require('./routes/categories');
const productCategoryRouter = require('./routes/productCategories');
const productOptionsRouter = require('./routes/productOptions');
const orderDetailsRouter = require('./routes/orderDetails');



const server = express();


server.set('views', path.join(__dirname, 'views'));


server.set('view engine', 'ejs');


server.use(express.static(path.join(__dirname, 'public')));


server.use('/', indexRouter);
server.use('/product', productRouter);
server.use('/customer', customerRouter);
server.use('/order', orderRouter);
server.use('/option', optionRouter);
server.use('/category', categoryRouter);
server.use('/product-category', productCategoryRouter);
server.use('/product-option', productOptionsRouter);
server.use('/order-detail', orderDetailsRouter);



server.use(function(req, res, next) {
  next(createError(404));
});


server.use(function(err, req, res, next) {
  
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  
  res.status(err.status || 500);
  res.render('error');
});


module.exports = server;
