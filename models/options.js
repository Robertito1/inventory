const ProductOption = require("./productOption")

module.exports = (sequelize, DataTypes) => {
  const Option = sequelize.define('Option', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      }
  });
  ProductOption.belongsTo(Option);
  Option.hasMany(ProductOption);
}