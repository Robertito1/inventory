const ProductCategory = require("./productCategory")

module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      },
     weight: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
     description: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
     image: {
        type: DataTypes.BLOB,
        allowNull: false,
      },
     created_dt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('NOW()'),
      },
  });
  Product.belongsTo(ProductCategory);
  ProductCategory.hasMany(Product)
}