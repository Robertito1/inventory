const Customer = require("./customer")
const Order_detail = require("./orderDetail")

module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4
      },
      amount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      },
     shipping_address: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
     order_email: {
        type: DataTypes.STRING,
        allowNull: false,
         validate: {
            notEmpty: true,
            isEmail: true
        }
      },
     order_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
  });
  Order.belongsTo(Customer);
  Customer.hasMany(Order)
  Order_detail.belongsTo(Order);
  Order.hasMany(Order_detail)
}