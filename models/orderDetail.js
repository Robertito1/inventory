const Order = require("./order")

module.exports = (sequelize, DataTypes) => {
  const Order_detail = sequelize.define('Order_detail', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4
      },
      product_id: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      },
     price: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
     quantity: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
  });
  Order_detail.belongsTo(Order);
  Order.hasMany(Order_detail)
}