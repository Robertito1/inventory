
const Product = require("/product");

module.exports = (sequelize, DataTypes) => {
  const ProductCategory = sequelize.define('ProductCategory', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4
      },
      product_is: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      },
     category_id: {
        type: DataTypes.STRING,
        allowNull: false,
      },
  });
  Product.belongsTo(ProductCategory);
  ProductCategory.hasMany(Product)
}