const Order = require("./order")

module.exports = (sequelize, DataTypes) => {
  const Customer = sequelize.define('Customer', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      },
     password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
     email: {
        type: DataTypes.STRING,
        allowNull: false,
         validate: {
            notEmpty: true,
            isEmail: true
        }
      },
     created_dt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('NOW()'),
      },
  });
  Order.belongsTo(Customer);
  Customer.hasMany(Order)
}