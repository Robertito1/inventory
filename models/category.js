const ProductCategory = require("./productCategory")

module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      },
     description: {
        type: DataTypes.TEXT,
        allowNull: false,
      }
  });
  ProductCategory.belongsTo(Category);
  Category.hasMany(ProductCategory)
}