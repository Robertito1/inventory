const Option = require("./option")

module.exports = (sequelize, DataTypes) => {
  const ProductOption = sequelize.define('ProductOption', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4
      },
      product_id: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      },
     option_id: {
       type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        }
      },
  });
 ProductOption.belongsTo(Option);
  Option.hasMany(ProductOption);
}